package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.R;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public DosenCRUDRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();

    }

    public DosenCRUDRecyclerAdapter(List<Dosen>dosenList){
        this.dosenList=dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);

        holder.tvNama.setText(d.getNama());
        holder.tvNidn.setText(d.getNidn());
        holder.tvAlamat.setText(d.getAlamat());
        holder.tvGelar.setText(d.getGelar());
        holder.tvEmail.setText(d.getEmail());
        holder.d = d;

    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvNama,tvNidn, tvGelar, tvAlamat,tvEmail,tvFoto;
        private RecyclerView rvDosen;
        Dosen d ;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNidn = itemView.findViewById(R.id.tvNidn);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvGelar = itemView.findViewById(R.id.tvGelar);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            rvDosen = itemView.findViewById(R.id.rvGetDsnAll);

        }
    }
}
