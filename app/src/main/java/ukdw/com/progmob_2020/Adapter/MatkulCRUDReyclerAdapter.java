package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.R;

public class MatkulCRUDReyclerAdapter extends RecyclerView.Adapter<MatkulCRUDReyclerAdapter.ViewHolder>{
    private Context context;
    private List<Matkul> matakuliahList;

    public MatkulCRUDReyclerAdapter(Context context) {
        this.context = context;
        matakuliahList = new ArrayList<>();
    }

    public MatkulCRUDReyclerAdapter(List<Matkul> matakuliahList) {
        this.matakuliahList = matakuliahList;
    }

    public void setMatakuliahList(List<Matkul> matakuliahList) {
        this.matakuliahList = matakuliahList;
        notifyDataSetChanged();
    }
    public List<Matkul> getMatakuliahList() {
        return matakuliahList;
    }

    @NonNull
    @Override
    public MatkulCRUDReyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul, parent, false);
        return new MatkulCRUDReyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MatkulCRUDReyclerAdapter.ViewHolder holder, int position) {
        Matkul mt = matakuliahList.get(position);

        holder.tvNama.setText(mt.getNama());
        holder.tvKode.setText(mt.getKode());
        holder.tvHari.setText(mt.getHari());
        holder.tvSesi.setText(mt.getSesi());
        holder.tvSks.setText(mt.getSks());

    }

    @Override
    public int getItemCount() {
        return matakuliahList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNama, tvKode, tvHari, tvSesi, tvSks;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);

        }
    }
}

