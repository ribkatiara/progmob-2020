package ukdw.com.progmob_2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class RecyclerActivity extends AppCompatActivity {
//recyler untuk menampung
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan); //deklarasi dulu
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        // generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Argo","72180101","082277401641");
        Mahasiswa m2 = new Mahasiswa("Halim","72180101","082277401641");
        Mahasiswa m3 = new Mahasiswa("Katon","72180101","082277401641");
        Mahasiswa m4 = new Mahasiswa("Siang","72180101","082277401641");
        Mahasiswa m5 = new Mahasiswa("Yetli","72180101","082277401641");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList); //utk memasukan list ke adapter

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);


    }
}