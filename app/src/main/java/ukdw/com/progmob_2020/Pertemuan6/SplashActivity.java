package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Window;
import android.widget.Toast;

import ukdw.com.progmob_2020.Crud.HomeActivity;
import ukdw.com.progmob_2020.Crud.LoginActivity;
import ukdw.com.progmob_2020.R;

public class SplashActivity extends AppCompatActivity {
    SharedPreferences session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Toast.makeText(SplashActivity.this, "Welcome User !", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            return;
        }
        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
            }
        };
        thread.start();
    }
}