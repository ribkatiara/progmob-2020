package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ukdw.com.progmob_2020.R;

public class HomeActivity extends AppCompatActivity {
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toast.makeText(HomeActivity.this, "Silahkan Di Pilih", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);

        if(session.getString("nimnik", "").isEmpty() && session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            return;
        }

        Button buttonMhs = (Button) findViewById(R.id.buttonMahasiswa);
        Button buttonMatkul = (Button) findViewById(R.id.buttonMatkul);
        Button buttonDsn = (Button) findViewById(R.id.buttonDosen);
        Button buttonLogout = (Button) findViewById(R.id.buttonLogout);




        buttonMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });
        buttonMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
        buttonDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = session.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}