package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button buttonGetDsn = (Button) findViewById(R.id.buttonGetDsn);
        Button buttonAddDsn = (Button) findViewById(R.id.buttonAddDsn);
        Button buttonHapusDsn = (Button) findViewById(R.id.buttonHapusDsn);
        Button buttonEditDsn = (Button) findViewById(R.id.buttonEditDsn);



        buttonGetDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });
        buttonAddDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });
        buttonHapusDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, HapusDosenActivity.class);
                startActivity(intent);
            }
        });
        buttonEditDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });
    }
}