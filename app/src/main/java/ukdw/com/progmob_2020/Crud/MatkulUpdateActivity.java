package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        EditText editKodeLama = (EditText)findViewById(R.id.editTextKodeLama);
        EditText editNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        EditText editKodeBaru = (EditText)findViewById(R.id.editTextKodeBaru);
        EditText editHariBaru = (EditText)findViewById(R.id.editTextHariBaru);
        EditText editSksBaru = (EditText)findViewById(R.id.editTextSksBaru);
        EditText editSesiBaru = (EditText)findViewById(R.id.editTextSesiBaru);
        Button btnUpdate= (Button)findViewById(R.id.btnUpdateDosen);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> hapus = service.delete_matkul(editKodeLama.getText().toString(),"72180246");
                hapus.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MatkulUpdateActivity.this,"Berhasil Dihapus",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"Gagal Dihapus",Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> call = service.add_matkul(
                        editNamaBaru.getText().toString(),
                        editKodeBaru.getText().toString(),
                        editHariBaru.getText().toString(),
                        editSksBaru.getText().toString(),
                        editSesiBaru.getText().toString(),
                        "72180246"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "GAGAL DISIMPAN", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });


    }
}