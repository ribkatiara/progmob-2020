package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        EditText editNidnLama = (EditText)findViewById(R.id.editTextNidnLama);
        EditText editNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        EditText editNidnBaru = (EditText)findViewById(R.id.editTextNidnBaru);
        EditText editAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaru);
        EditText editGelarBaru = (EditText)findViewById(R.id.editTextGelarBaru);
        EditText editEmailBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        Button btnUpdate= (Button)findViewById(R.id.btnUpdateDsn);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> hapus = service.delete_dsn(editNidnLama.getText().toString(),"72180246");
                hapus.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(DosenUpdateActivity.this,"Berhasil Dihapus",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this,"Gagal Dihapus",Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> call = service.add_dsn(
                        editNamaBaru.getText().toString(),
                        editNidnBaru.getText().toString(),
                        editAlamatBaru.getText().toString(),
                        editGelarBaru.getText().toString(),
                        editEmailBaru.getText().toString(),
                        "kosongkan saja diisi bebas karena dirandom sistem",
                        "72180246"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "GAGAL DISIMPAN", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

    }
}